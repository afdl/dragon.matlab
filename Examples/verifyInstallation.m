% This example script verifies that Dragon is functioning as intended.
clear variables;


%% SETUP

addpath(genpath( '/home/chris/code/dragon/matlab' ));  % Add Dragon functions to MATLAB path
dragonFilePath = '/home/chris/code/dragon/library';    % Path to dragon.dll in Windows or libdragon.so in Linux
mainDir = dirchk('/home/chris/tmp');


%% CONFIGURE AND INITIALIZE DRAGON

% Load the Dragon Library
isDragonLoaded = DragonLoadLibrary(dragonFilePath);
if ~isDragonLoaded, error('Dragon library was not successfully loaded.'); end

% Get reasonable default settings and configure camera
settings = DragonGetDefaultSettings();
settings.camera(1) = DragonConfigureCamera( 'b6640','hexa77', ...
	'Magnification',-0.5, 'focalLength',60, 'fNumber',2.67 );

% Maximum verboseness in case of failure
settings.processing.logFile  = fullfile(mainDir,'dragon.log');
settings.processing.logLevel = 5;

% Volume coordinates [mm]
settings.volume.x = -21 : 0.20 : 21;
settings.volume.y = -21 : 0.20 : 21;
settings.volume.z = -21 : 0.20 : 21;

% Simulation settings
settings.simulation.overwrite = true;
settings.simulation.camAngles = [0 0];
settings.simulation.nRays     = 1000;

% Refocusing algorithm (overwritten later)
settings.reconstruction.algorithm = 'integral';

% PIV windows
settings.crossCorrelation.windowSizes   = [32,16,12,12];	% Correlation window sizes [voxels]
settings.crossCorrelation.windowOffsets = [16, 8, 4, 4];	% Correlation window offsets [voxels]

settings.filepath.mcal    = mainDir;
settings.filepath.lfcal   = mainDir;
settings.filepath.images  = dirchk(fullfile(mainDir,'images'));
settings.filepath.volumes = dirchk(fullfile(mainDir,'volumes'));
settings.filepath.vectors = dirchk(fullfile(mainDir,'vectors'));

% Save settings to file and initialize from that file
settingsFile = fullfile(mainDir,'settings.json');
DragonWriteSettingsFile(settingsFile,settings);
DragonInitFromSettingsFile(settingsFile);


%% GENERATE PARTICLE FIELD PAIR

fprintf('Generating random vortex ring... ');
[A,B] = DragonParticleFieldGeneration( 10000, ...
	settings.volume.x, settings.volume.y, settings.volume.z, ...
	'ring_vortex', 2, 0,0,0, 3, 0,0,0 );
fprintf('done.\n');

fh = figure; title('Vortex Ring');
plot3( [A.x(:) B.x(:)]', [A.y(:) B.y(:)]', [A.z(:) B.z(:)]' ); axis tight; drawnow;
pause(2); close(fh);


%% GENERATE SYNTHETIC IMAGES

fprintf('Synthesizing image A... ');
A.im = DragonSimulateImage( A.x,A.y,A.z,A.d,A.i, 0, 6600,4400 );
imwrite( uint16(A.im), dirchk(fullfile(settings.filepath.images,'camera0','0000.tif')) );
fprintf('done.\n');

fprintf('Synthesizing image B... ');
B.im = DragonSimulateImage( B.x,B.y,B.z,B.d,B.i, 0, 6600,4400 );
imwrite( uint16(B.im), dirchk(fullfile(settings.filepath.images,'camera0','0001.tif')) );
fprintf('done.\n');

clear A B


%% GENERATE SYNTHETIC LFCAL

fprintf('Synthesizing light field calibration... ');
DragonMonteCarloDLFC();
fprintf('done.\n');


%% BUILD RADIANCE ARRAY

fprintf('Building radiance arrays... ');
raw(:,:,1) = imread(fullfile(settings.filepath.images,'camera0','0000.tif'));
raw(:,:,2) = imread(fullfile(settings.filepath.images,'camera0','0001.tif'));
DragonBuildRadiance( fullfile(settings.filepath.mcal,'camera0.drg-mcal'), raw );
fprintf('done.\n');


%% GENERATE PERSPECTIVE IMAGES

% Aperture diamater (with some padding)
D = settings.camera(1).focalLength / settings.camera(1).fNumber - 3;

% Draw a circle near the edge of the aperture
h = 0:10:359;
u = D/2*sind(h);
v = D/2*cosd(h);

x = linspace( -settings.camera(1).pixelCount(1)*settings.camera(1).pixelPitch/2, ...
	settings.camera(1).pixelCount(1)*settings.camera(1).pixelPitch/2, 900 );
y = linspace( -settings.camera(1).pixelCount(2)*settings.camera(1).pixelPitch/2, ...
	settings.camera(1).pixelCount(2)*settings.camera(1).pixelPitch/2, 600 );

fprintf('Generating perspective views... ');
persp = DragonGeneratePerspectiveViews( x,y, u,v, 2 );
fprintf('done.\n');

% Normalize (for convenience only)
persp = persp / max(persp(:));
lims = stretchlim( persp(:), [0 0.99] );

% Display
fh = figure; title('Perspective Sweep');
for uv = 1:length(u)
	rgb(:,:,3) = imadjust( persp(:,:,uv,1)', lims );	% Frame A in blue (head lights)
	rgb(:,:,1) = imadjust( persp(:,:,uv,2)', lims );	% Frame B in red (tail lights)
	
	imshow(rgb); drawnow;
end
close(fh);

clear persp


%% GENERATE REFOCUSED IMAGES

alpha = logspace( log10(0.95), log10(1.07), 35 );

fprintf('Generating focal stack... ');
stack = DragonRefocusToFocalStack( alpha, 900,600, 2 );
fprintf('done.\n');

% Normalize (for convenience only)
stack = stack / max(stack(:));
lims = stretchlim( stack(:), [0 0.99] );

% Display
fh = figure;
for a = 1:length(alpha)
	rgb(:,:,3) = imadjust( stack(:,:,a,1)', lims );		% Frame A in blue (head lights)
	rgb(:,:,1) = imadjust( stack(:,:,a,2)', lims );		% Frame B in red (tail lights)
	
	imshow(rgb); drawnow;
end
close(fh);

clear stack


%% PERFORM VOLUMETRIC RECONSTRUCTION

settings.reconstruction.algorithm   = 'mart';
settings.reconstruction.nIterations = 5;

% Save and re-initialize for changes to take effect
DragonWriteSettingsFile(settingsFile,settings);
DragonInitFromSettingsFile(settingsFile);

fprintf('Reconstructing volume A... ');
DragonLightFieldReconstruction( 0000 );
fprintf('done.\n');

fprintf('Reconstructing volume B... ');
DragonLightFieldReconstruction( 0001 );
fprintf('done.\n');


%% PERFORM 3D PIV

fprintf('Cross-correlating volumes... ');
DragonRunCrossCorrelation3D( 0000, 0001 );
fprintf('done.\n');

DragonQuit();
