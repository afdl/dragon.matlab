% This example script demonstrates how to perform volumetric reconstructions using `DragonLightFieldReconstruction`.
% This function is highly streamlined, but has ridgid input requirements. Input images must be named 0000.tif, 0001.tif,
% 0002.tif, etc. and be placed in folders named camera0, camera1, etc. Microlenses calibrations must be named
% camera0.drg-mcal, camera1.drg-mcal, etc. Light-field calibrations must be named camera0.drg-lfcal, camera1.drg-lfcal,
% etc. This method is mandatory for multi-camera reconstructions, but also works with single camera setups.
clear variables;


%% PATH DEFINITIONS

addpath(genpath( 'C:\code\dragon\matlab' ));  % Add Dragon functions to MATLAB's path.
dragonFilePath = 'C:\code\dragon\redist';     % Define path to dragon.dll in Windows or libdragon.so in Linux.

mainDir = 'D:\project\data';                  % Define path to project folder.


%% SETUP

imStart    = 0000;  % Image number to start on (typically 0)
imEnd      = 0399;  % Image number to end on
imPerBatch =   10;  % Number of images to reconstruct simultaneously. This should be as large as your RAM allows.

% This variable specifies the pairwise [x,y] camera rotations in radians to perform a Monte Carlo simulation for a
% synthetic light-field calibration (lfcal). This is required to be run at least once for synthetic data or for real
% data that has no spatial (e.g. dot card) calibration. If you already have lfcal files, from a real calibration or
% from running this script once already, set this variable to 0 or [] to skip the Monte Carlo simulation. For more
% information on performing light-field calibration. See DragonCalibrateLightfield.m in the Calibration folder.
camAngles = [ 0 -45, 0 45 ]*(pi/180);

% These variables define the grid upon which particles will be reconstructed. For the largest possible volume, set the
% limits slightly larger than the field of view and use a resolution of 450x300x300 or more. Note that your calibrated
% origin might not be at the center of your volume! Although the reconstruction can result in particle elongation
% (depending on your camera setup), it is still recommended to use cubic voxels. Here we are using 6 voxels per mm for
% a final grid of 481x301x301. You can also directly define the grid using the colon operator or linspace.
x = [ -40, 40, 1/6 ];
y = [ -25, 25, 1/6 ];
z = [ -25, 25, 1/6 ];


%% CONFIGURE AND INITIALIZE DRAGON

% Load the Dragon Library.
isDragonLoaded = DragonLoadLibrary(dragonFilePath);
if ~isDragonLoaded, error('Dragon library was not successfully loaded.'); end

% Get reasonable default settings and configure all cameras.
settings = DragonGetDefaultSettings();
settings.camera(1:2) = DragonConfigureCamera( ...
	'b6640', ...                % Camera body: Imperx Bobcat B6640
	'hexa77', ...               % Microlens array: Hexagonal MLA with 77 um pitch
	'Magnification', -0.5, ...  % Magnification at the nominal focal plane
	'focalLength', 85, ...      % Main lens focal length
	'fNumber', 2.67 );          % Main lens f-number

% Maximize verboseness in case of failure.
settings.processing.logFile  = fullfile(mainDir,'dragon.log');
settings.processing.logLevel = 5;

% Configure simulation. (optional)
settings.simulation.camAngles = camAngles;         % Pairwise [x,y] camera angles in radians for simulating lfcal.

% Configure light-field generation.
settings.lightField.useSlidingMeanFilter = true;   % Apply a sliding mean filter to each perspective? This greatly
                                                   % improves reconstruction quality of particle fields.

% Configure volumetric reconstruction.
settings.reconstruction.algorithm       = 'mart';  % Tomographic algorithm to use. MART is typical for particle fields.
settings.reconstruction.nIterations     = 7;       % Number of MART iterations to perform.
settings.reconstruction.relaxation      = 0.5;     % The relaxation factor for each iteration (see MART literature).
settings.reconstruction.voxelCutoff     = 1e-5;    % Intensity value for which we consider the voxel effectively zero.
                                                   % This accelerates the reconstruction by skipping empty voxels.
settings.reconstruction.outputEveryIter = false;   % Save a volume after each iteration? Useful if you haven't
                                                   % determined the number of iterations and relaxation factor yet.

% Configure volumetric masking (if any).
settings.reconstruction.maskingScheme = 'static';  % Masking scheme to use. Static masking uses the convex hull formed
                                                   % by the intersection of all views from all cameras.
settings.reconstruction.outputMasks   = false;     % Output volume masks for inspection?

% Define volume coordinates in mm.
settings.volume.x = x;
settings.volume.y = y;
settings.volume.z = z;

% Define filepath locations.
settings.filepath.mcal    = mainDir;
settings.filepath.lfcal   = mainDir;
settings.filepath.images  = fullfile(mainDir,'images');
settings.filepath.volumes = fullfile(mainDir,'volumes');

% Save settings to file, then initialize from that file.
settingsFile = fullfile(mainDir,'settings.json');
DragonWriteSettingsFile(settingsFile,settings);
DragonInitFromSettingsFile(settingsFile);


%% PRE-PROCESS IMAGES
% It is recommended that the images to be reconstructed look like a starry night sky: bright dots on a dark background,
% with small but discernable space between dots. If the raw images have substantial amounts of laser flare or ambient
% light, then background subtraction will be necessary. The preferred method of background subtraction is to acquire an
% in situ reference image with the laser enabled, but no seed particles. If an in situ background is impossible (can't
% remove seeding) or insufficient (significant particle scattering), a representative background may be generated by
% calculating the median of a large number of images. In either case, it is recommended to treat each laser pulse
% independently to account for minor differences in illumination.


%% RECONSTRUCT

nBatches = ceil((imEnd-imStart+1)/imPerBatch);
for batch=1:nBatches

	% Create a list of images to be reconstructed into volumes this batch.
	imList = ( imStart+(batch-1)*imPerBatch : min(imStart+batch*imPerBatch-1,imEnd) );

	% Perform the volumetric reconstruction! This function does all of the heavy lifting.
	DragonLightFieldReconstruction( imList );

	% Convert the raw binaries into TecPlot binaries for inspection. (optional)
	for i=1:length(imList)
		vol = DragonLoadVolume( fullfile(settings.filepath.volumes,sprintf('%04d.bin',imList(i))) );
		DragonOutputVolume( (vol.E-min(vol.E(:)))/range(vol.E(:)), fullfile(settings.filepath.volumes,sprintf('%04d.plt',imList(i))) );
	end

end
