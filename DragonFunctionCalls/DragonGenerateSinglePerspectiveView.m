function [perspectiveImages] = DragonGenerateSinglePerspectiveView(xVec,yVec,uVal,vVal,nImages)
%-------------------------------------------------------------------------------------
% function [perspectiveImages] = DragonGenerateSinglePerspectiveView(xVec,yVec,uVal,vVal,nImages)
%
% Purpose: Generates a perspective view from the light field
% Method:  Calls into Dragon.dll, using DRG_generateSinglePerspectiveView()
%          as defined in Dragon.h
% 
% INPUTS:       xVec:    vector of x-coordinates (object space)
%               yVec:    vector of y-coordinates (object space)
%               uVal:    x aligned position on main lens [mm]
%               vVal:    y aligned position on main lens [mm] 
%               nImages: number of images used to build radiance (default = 1)
%                
% OUTPUTS:      perspectiveImages: image generated using perspective (uVal,vVal,nImages)   
%-------------------------------------------------------------------------------------
if nargin == 4
    nImages = 1;
end

if (libisloaded('Dragon'))
    
   iOut = libpointer('singlePtr',zeros(length(xVec)*length(yVec)*nImages,1));
   xp   = libpointer('singlePtr',xVec);
   yp   = libpointer('singlePtr',yVec);
   
   calllib('Dragon','DRG_generateSinglePerspectiveView',xp,length(xVec),yp,length(yVec),uVal,vVal,iOut);
   
   perspectiveImages = iOut.val;
   perspectiveImages = reshape(perspectiveImages,length(xVec),length(yVec),nImages);
   
   clear xp;
   clear yp;
   clear iOut;
   
end