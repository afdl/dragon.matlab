function [] = DragonLightFieldReconstruction(imageNumList)
%-------------------------------------------------------------------------------------
% function [] = DragonLightFieldReconstruction(imageNumList)
%
% Purpose: Reconstruct a light field to a volume
% Method:  Calls into Dragon.dll, using DRG_lightFieldReconstruction()
%          as defined in Dragon.h
% 
% INPUTS:       imageNumList:       list of images to be loaded from image_folder.
%-------------------------------------------------------------------------------------
if (libisloaded('Dragon'))
    
   imList = libpointer('int32Ptr',imageNumList);
   nImages = length(imageNumList);
   
   calllib('Dragon','DRG_lightFieldReconstruction',imList,nImages);

   clear imList;
   
end
