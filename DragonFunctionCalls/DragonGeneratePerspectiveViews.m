function [perspectiveImages] = DragonGeneratePerspectiveViews(xVec,yVec,uVec,vVec,nImages)
%-------------------------------------------------------------------------------------
% function [perspectiveImages] = DragonGeneratePerspectiveViews(xVec,yVec,uVec,vVec,nImages)
%
% Purpose: Generates a perspective sweep from the light field
% Method:  Calls into Dragon.dll, using DRG_generatePerspectiveViews()
%          as defined in Dragon.h
% 
% INPUTS:       xVec: vector of x-coordinates (object space)
%               yVec: vector of y-coordinates (object space)
%               uVec: vector of x aligned positions on main lens [mm]
%               vVec: vector of y aligned positions on main lens [mm]
%               nImages:  number of images used to build radiance (default = 1)
%                
% OUTPUTS:      perspectiveImages: set of images (x,y,n), where n is the image number   
%-------------------------------------------------------------------------------------
if nargin == 4
    nImages = 1;
end

if (libisloaded('Dragon'))
    
   iOut = libpointer('singlePtr',zeros(length(xVec)*length(yVec)*length(uVec)*nImages,1));
   xp   = libpointer('singlePtr',xVec);
   yp   = libpointer('singlePtr',yVec);
   up   = libpointer('singlePtr',uVec);
   vp   = libpointer('singlePtr',vVec);
   
   calllib('Dragon','DRG_generatePerspectiveViews',xp,length(xVec),yp,length(yVec),up,vp,length(uVec),iOut);
   
   perspectiveImages = iOut.val;
   perspectiveImages = reshape(perspectiveImages,length(xVec),length(yVec),length(uVec),nImages);
   
   clear xp;
   clear yp;
   clear up;
   clear vp;
   clear iOut;
   
end