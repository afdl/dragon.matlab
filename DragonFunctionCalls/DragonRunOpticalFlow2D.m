function [] = DragonRunOpticalFlow2D(numA,numB,outputFileName)
%-------------------------------------------------------------------------------------
% [] = DragonRunOpticalFlow2D(numA,numB,outputFileName)
%
% Purpose: Optical flow on an image pair
% Method:  Calls into Dragon.dll, using DRG_runOpticalFlow2D()
%          as defined in Dragon.h
% 
% INPUTS:       numberA:         number corresponding to first frame (t)
%               numberB:         number corresponding to second frame (t + dt)
%               outputFileName:  plt file for exporting data
%                
% OUTPUTS:      none (a file is saved from within dragon)
%-------------------------------------------------------------------------------------
if (libisloaded('Dragon'))
	
	if nargin<3, outputFileName = []; end
   
	calllib('Dragon','DRG_runOpticalFlow2D',numA,numB,outputFileName);

end