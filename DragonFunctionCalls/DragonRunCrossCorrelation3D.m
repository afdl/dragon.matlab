function [] = DragonRunCrossCorrelation3D(numA,numB,outputFileName)
%-------------------------------------------------------------------------------------
% [] = DragonRunCrossCorrelation3D(numA,numB,outputFileName)
%
% Purpose: 3D Cross-correlation (for PIV) 
% Method:  Calls into Dragon.dll, using DRG_runCrossCorrelation3D()
%          as defined in Dragon.h
% 
% INPUTS:       numberA:         number corresponding to first frame (t)
%               numberB:         number corresponding to second frame (t + dt)
%               outputFileName:  plt file for exporting data
%                
% OUTPUTS:      none (a file is saved from within dragon)
%
% NOTABLE SETTINGS:
%   - useMask:                    use a volume mask?
%   - useCrop:                    crop volume?
%   - cropX, cropY, cropZ:        crop limits in x,y,z
%   - limitsU, limitsV, limitsW:  velocity limits in x,y,z
%   - nValidationIterations:      number of vector validation/replacement
%                                 iterations
%   - validationThreshold         normalized median test validation threshold
%   - weightingFunction:          UNIFORM, GAUSSIAN
%   - deformationScheme:          NONE, BILINEAR, SINC7, SINC11
%-------------------------------------------------------------------------------------
if (libisloaded('Dragon'))

	if nargin<3, outputFileName = []; end
   
	calllib('Dragon','DRG_runCrossCorrelation3D',numA,numB,outputFileName);
	pause(2); % stops matlab from crashing???
   
end