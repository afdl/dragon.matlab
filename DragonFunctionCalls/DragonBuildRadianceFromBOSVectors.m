function [] = DragonBuildRadianceFromBOSVectors(vectorDir,xVec,yVec,uVec,vVec,type)
%-------------------------------------------------------------------------------------
% [] = DragonBuildRadianceFromBOSVectors(vectorDir,xVec,yVec,uVec,vVec,type)
%
% Purpose: Builds the radiance arrays (x,y,u,v,rad) used to generate
%          refocused  BOS data
% Method:  Calls into Dragon.dll, using DRG_buildRadianceFromBOSVectors()
%          as defined in Dragon.h
% 
% INPUTS:       vectorDir: folder that containes vectors associated with 
%                          perspective views
%               xVec:      coordinates of the perspective images x-dir
%               xVec:      coordinates of the perspective images y-dir
%               uVec:      u-values of the generated perspective images
%               vVec:      v-values of the generated perspective images
%               type:      how to build the radiance array  (0 - U_VECTORS,
%                                                            1 - V_VECTORS, 
%                                                            2 - MAGNITUDE)
%                
% OUTPUTS:      none
%-------------------------------------------------------------------------------------
if (libisloaded('Dragon'))
    
   x_ptr = libpointer('singlePtr',xVec);
   y_ptr = libpointer('singlePtr',yVec);
   u_ptr = libpointer('singlePtr',uVec(:));
   v_ptr = libpointer('singlePtr',vVec(:));
   
   calllib('Dragon','DRG_buildRadianceFromBOSVectors',vectorDir,x_ptr,y_ptr,u_ptr,size(uVec,1),v_ptr,size(uVec,2),type);
   
   clear x_ptr;
   clear y_ptr;
   clear u_ptr;
   clear v_ptr;
end