function [refocusedImage] = DragonRefocusToImage(alpha,nVoxelsX,nVoxelsY,nImages)
%-------------------------------------------------------------------------------------
% function [refocusedImage] = DragonRefocusToImage(alpha,nVoxelsX,nVoxelsY,nImages)
%
% Purpose: Refocus a light field to an image using shift-invariant refocusing.
% Method:  Calls into Dragon.dll, using DRG_refocusLightFieldToImage()
%          as defined in Dragon.h
% 
% INPUTS:       alpha:    alpha value
%               nVoxelsX: number of pixels in output images x-dir
%               nVoxelsY: number of pixels in output images y-dir
%               nImages:  number of images used to build radiance (default = 1)
%                
% OUTPUTS:      refocusedImage: image of refocused intensity indexed by (x,y)
%
% NOTABLE SETTINGS:
%   - algorithm:                          0 - INTEGRAL, 1 - FILTERED
%   - intensityThreshold (filtered only): minimum intensity to consider a
%                                         pixel valid
%   - filterThreshold (filtered only):    Validity threshold, all voxels under 
%                                         this value are set to zero.   
% NOTES:
%   - The span of x,y is goverened by the calibration data.
%-------------------------------------------------------------------------------------
if nargin == 3
    nImages = 1;
end

if (libisloaded('Dragon'))
    
   iOut = libpointer('singlePtr',zeros(nVoxelsX*nVoxelsY*nImages,1));
   
   calllib('Dragon','DRG_refocusLightFieldToImage',alpha,nVoxelsX,nVoxelsY,iOut);
   
   refocusedImage = iOut.val;
   refocusedImage = reshape(refocusedImage,nVoxelsX,nVoxelsY,nImages); 
   
   clear iOut;
end
