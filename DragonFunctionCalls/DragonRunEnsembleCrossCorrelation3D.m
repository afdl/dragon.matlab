function [] = DragonRunEnsembleCrossCorrelation3D(numList,outputFileName)
%-------------------------------------------------------------------------------------
% [] = DragonRunEnsembleCrossCorrelation3D(numList,outputFileName)
%
% Purpose: 3D Cross-correlation (for PIV) 
% Method:  Calls into Dragon.dll, using DRG_runCrossCorrelation3D()
%          as defined in Dragon.h
% 
% INPUTS:       numList:         list of frame pairs
%               outputFileName:  plt file for exporting data
%                
% OUTPUTS:      none (a file is saved from within dragon)
%
% NOTABLE SETTINGS:
%   - useMask:                    use a volume mask?
%   - useCrop:                    crop volume?
%   - cropX, cropY, cropZ:        crop limits in x,y,z
%   - limitsU, limitsV, limitsW:  velocity limits in x,y,z
%   - nValidationIterations:      number of vector validation/replacement
%                                 iterations
%   - validationThreshold         normalized median test validation threshold
%   - weightingFunction:          UNIFORM, GAUSSIAN
%   - deformationScheme:          NONE, BILINEAR, SINC7, SINC11
%-------------------------------------------------------------------------------------
if (libisloaded('Dragon'))

	if nargin<2, outputFileName = []; end

	numList_ptr = libpointer('int32Ptr',volList);

   
	calllib('Dragon','DRG_runCorrelationAveraging3D',numList_ptr,numel(numList),outputFileName);
	pause(2); % stops matlab from crashing???
   
	clear numList_ptr;
   
end