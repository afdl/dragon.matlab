function DragonMonteCarloDLFC()
%-------------------------------------------------------------------------------------
% function DragonMonteCarloDLFC()
%
% Purpose: Simulate DLFC for all defined cameras 
% Method:  Calls into Dragon.dll, using DRG_monteCarloDLFC()
%          as defined in Dragon.h
%-------------------------------------------------------------------------------------

if (libisloaded('Dragon'))
    
    calllib('Dragon','DRG_monteCarloDLFC');
    
end
