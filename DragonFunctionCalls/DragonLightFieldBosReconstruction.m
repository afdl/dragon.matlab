function [] = DragonLightFieldBosReconstruction(xVecBos,yVecBos,uVec,vVec,type,xVecVol,yVecVol,zVecVol,camAngles,nCameras,bos_folder,lfcal_folder,vol_folder)
%-------------------------------------------------------------------------------------
% function [] = DragonLightFieldBosReconstruction(xVec,yVec,zVec,camAngles,imageNumList,
%                                                 image_folder,mcal_folder,lfcal_folder,vol_folder)
%
% Purpose: Reconstruct a light field to a volume
% Method:  Calls into Dragon.dll, using DRG_lightFieldReconstruction()
%          as defined in Dragon.h
% 
% INPUTS:       xVecBos:            coordinates of the perspective images x-dir
%               yVecBos:            coordinates of the perspective images y-dir
%               uVec:               u-values of the generated perspective images
%               vVec:               v-values of the generated perspective images
%               type:               how to build the radiance array  (0 - U_VECTORS,
%                                                                     1 - V_VECTORS,
%                                                                     2 - MAGNITUDE)
%               xVecVol:            vector of x-coordinates (object space)
%               yVecVol:            vector of y-coordinates (object space)
%               zVecVol:            vector of z-coordinates (object space)
%               camAngles:          vector of camera angles (x1,y1,x2,y2,...) 
%                                   only used for data without lfcal
%               nCameras:           number of cameras
%               imageNumList:       list of images to be loaded from image_folder.
%               bos_folder:         folder containing camera0, camera1, ... 
%                                   subfolders where the images 0000.plt, 0001.plt, ... are located.
%               mcal_folder:        folder containing camera0.drg-mcal, camera1.drg-mcal, ...
%               lfcal_folder:       folder containing camera0.drg-lfcal, camera1.drg-lfcal, ...
%               vol_folder:         folder to output the volumes to.
%                
% OUTPUTS:      none
%-------------------------------------------------------------------------------------
if (libisloaded('Dragon'))
	
	xb_ptr   = libpointer('singlePtr',xVecBos);
	yb_ptr   = libpointer('singlePtr',yVecBos);
	u_ptr    = libpointer('singlePtr',uVec(:));
	v_ptr    = libpointer('singlePtr',vVec(:));
	
	nVoxelsX = length(xVecVol);
	nVoxelsY = length(yVecVol);
	nVoxelsZ = length(zVecVol);
	
	xv_ptr   = libpointer('singlePtr',xVecVol);
	yv_ptr   = libpointer('singlePtr',yVecVol);
	zv_ptr   = libpointer('singlePtr',zVecVol);
	
	if numel(camAngles) < 2
		cA = [];
	else
		cA = libpointer('singlePtr',camAngles);
	end
	
	calllib('Dragon','DRG_lightFieldBosReconstruction',xb_ptr,yb_ptr,u_ptr,size(uVec,1),v_ptr,size(uVec,2),type,xv_ptr,nVoxelsX,yv_ptr,nVoxelsY,zv_ptr,nVoxelsZ,cA,nCameras,bos_folder,lfcal_folder,vol_folder);
	
	clear *_ptr cA;
end
