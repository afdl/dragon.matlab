function DragonCalibrateSyntheticCamera(xPos,yPos,zPos,rotAngleX,rotAngleY,camID,lfcalFile)
%-------------------------------------------------------------------------------------
% function DragonCalibrateSyntheticCamera(xPos,yPos,zPos,xAngle,yAngle,lfcalFile)
%
% Purpose: Simulate an image of a particle field using a plenoptic camera  
% Method:  Calls into Dragon.dll, using DRG_calibrateSyntheticCamera()
%          as defined in Dragon.h
% 
% INPUTS:       xPos:        vector of control x-coordinates (object space)
%               yPos:        vector of control y-coordinates (object space)
%               zPos:        vector of control z-coordinates (object space)
%               rotAngleX:   camera rotation angle about x-axis
%               rotAngleY:   camera rotation angle about y-axis
%               lfcalFile:   output filename for calibration data
% NOTABLE SETTINGS:
%   - nRays: number of rays to be simulated for each particle.
%-------------------------------------------------------------------------------------

if (libisloaded('Dragon'))
    
    xp = libpointer('singlePtr',xPos);
    yp = libpointer('singlePtr',yPos);
    zp = libpointer('singlePtr',zPos);
    fp = libpointer('string',lfcalFile);
    calllib('Dragon','DRG_calibrateSyntheticCamera',length(xPos),xp,yp,zp,rotAngleX,rotAngleY,camID,fp);
    
    clear xp;
    clear yp;
    clear zp;
    clear fp;
    
end
