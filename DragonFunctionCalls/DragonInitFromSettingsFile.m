function [] = DragonInitFromSettingsFile(fileName)
%-------------------------------------------------------------------------------------
% function [] = DragonInitFromSettingsFile(fileName)
%
% Purpose: Initializes the Dragon library from a settigns file
% Method:  Calls into Dragon.dll, using DRG_initFromSettingsFile()
%          as defined in Dragon.h
% 
% INPUTS:       fileName: filename of input settings file
%                
% OUTPUTS:      none
%
% NOTES:
%   - This function can be called more than once to reset the settings. If
%     this is done:
%       - The volumetric calibration data will be reset.
%       - The light field data will be erased.
%-------------------------------------------------------------------------------------

if (libisloaded('Dragon'))
    calllib('Dragon','DRG_initFromJSON',fileName);
end