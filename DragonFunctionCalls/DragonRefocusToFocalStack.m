function [focalStack] = DragonRefocusToFocalStack(alphaVec,nVoxelsX,nVoxelsY,nImages)
%-------------------------------------------------------------------------------------
% function [focalStack] = DragonRefocusToFocalStack(alphaVec,nVoxelsX,nVoxelsY,nImages)
%
% Purpose: Refocus a light field to a focal stack using shift-invariant refocusing.
% Method:  Calls into Dragon.dll, using DRG_refocusLightFieldToFocalStack()
%          as defined in Dragon.h
% 
% INPUTS:       alphaVec: vector of alpha values
%               nVoxelsX: number of pixels in output images x-dir
%               nVoxelsY: number of pixels in output images y-dir
%               nImages:  number of images used to build radiance (default = 1)
%                
% OUTPUTS:      focalStack: focalStack of refocused intensity indexed by (x,y,alpha)
%
% NOTABLE SETTINGS:
%   - algorithm:                          0 - INTEGRAL, 1 - FILTERED
%   - intensityThreshold (filtered only): minimum intensity to consider a
%                                         pixel valid
%   - filterThreshold (filtered only):    Validity threshold, all voxels under 
%                                         this value are set to zero.   
% NOTES:
%   - The span of x,y is goverened by the calibration data.
%-------------------------------------------------------------------------------------
if nargin == 3
    nImages = 1;
end

if (libisloaded('Dragon'))
    
   iOut = libpointer('singlePtr',zeros(nVoxelsX*nVoxelsY*length(alphaVec)*nImages,1));
   ap   = libpointer('singlePtr',alphaVec);
  
   calllib('Dragon','DRG_refocusLightFieldToFocalStack',ap, length(alphaVec),nVoxelsX,nVoxelsY,iOut);
   
   focalStack = iOut.val;
   focalStack = reshape(focalStack,nVoxelsX,nVoxelsY,length(alphaVec),nImages);
   
   clear ap;
   clear iOut;
   
end