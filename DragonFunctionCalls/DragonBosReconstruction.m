function [] = DragonBosReconstruction(imageNumber)
%-------------------------------------------------------------------------------------
% function [] = DragonBosReconstruction(imageNumber)
%
% Purpose: Reconstruct an index field using tomographic BOS
% Method:  Calls into Dragon.dll, using DRG_bosReconstruction() as defined in Dragon.h
%
% INPUTS:  imageNumber:  four digit image number to reconstruct
%-------------------------------------------------------------------------------------
if (libisloaded('Dragon'))
   calllib('Dragon','DRG_bosReconstruction',imageNumber);
end
