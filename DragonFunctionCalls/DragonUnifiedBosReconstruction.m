function [] = DragonUnifiedBosReconstruction(imageNumber)
%-------------------------------------------------------------------------------------
% function [] = DragonUnifiedBosReconstruction(imageNum)
%
% Purpose: Reconstruct an index field using Unified BOS
% Method:  Calls into Dragon.dll, using DRG_unifiedBosReconstruction()
%          as defined in Dragon.h
%
% INPUTS:  imageNumber:  four digit image number to reconstruct
%-------------------------------------------------------------------------------------
if (libisloaded('Dragon'))
   calllib('Dragon','DRG_unifiedBosReconstruction',imageNumber);
end
