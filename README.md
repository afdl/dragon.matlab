#### Dragon Library: MATLAB Wrappers ####

This is a collection of MATLAB wrapper scripts for the Dragon Library. Dragon is a compiled C library for plenoptic reconstruction and cross-correlation; it is available only by request.

Originally developed by Tim Fahringer. Currently developed and maintained by Chris Clifford. Contact Brian Thurow <thurow@auburn.edu> with any questions, comments, or concerns regarding this collection.

