function c = DragonConfigureCamera( varargin )
% Automatically set camera parameters based on configuration string.
%
%	settings.camera(1) = DragonConfigureCamera( 'b6640','hexa77', ...
%		'magnification',-0.75, 'focalLength',60, 'fNumber',2.3 );
%
%	settings.camera(2) = DragonConfigureCamera( 'veo4k','hexa35.5',
%		'relayMagnification', 70/50, ... );


% Create fully defined struct. Makes MATLAB happy.
c = struct( 'magnification',[], 'focalLength',[], 'fNumber',[], ...
	'pixelCount',[], 'pixelPitch',[], 'microlensType','none', ...
	'microlensCount',[], 'microlensPitch',[], 'microlensFocalLength',[] );

% Loop through input arguments
for k=1:nargin
	if ischar(varargin{k})
		switch lower(varargin{k})

			% Optics
			case {'magnification','mag','m'}
				c.magnification = varargin{k+1};

			case {'focallength','f_m','f'}
				c.focalLength = varargin{k+1};

			case {'f-number','fnumber','fnum_m','fnum','f#'}
				c.fNumber = varargin{k+1};

			case {'relaymagnification','relay','r'}
				if isempty(c.pixelPitch), error('Sensor must be specified before relay magnification.'); end
				c.pixelPitch = c.pixelPitch / varargin{k+1};

			% Sensor
			case 'b4020'     % Imperx Bobcat B4020, 11 MP
				c.pixelCount = [4032 2688];
				c.pixelPitch = 0.0090;

			case 'b4820'     % Imperx Bobcat B4820, 16 MP
				c.pixelCount = [4904 3280];
				c.pixelPitch = 0.0074;

			case {'b6620','b6640'} % Imperx Bobcat B66x0, 29 MP
				c.pixelCount = [6600 4400];
				c.pixelPitch = 0.0055;

			case 'veo640'    % Vision Research Phantom VEO 640, 4 MP
				c.pixelCount = [2560 1600];
				c.pixelPitch = 0.0010;

			case 'veo4k'     % Vision Research Phantom VEO 4K, 9 MP
				c.pixelCount = [4096 2304];
				c.pixelPitch = 0.0068;

			% Microlens Array
			case 'rect125'
				c.microlensType        = 'rect';
				c.microlensCount       = [289 193];
				c.microlensPitch       = 0.1250;
				c.microlensFocalLength = 0.5000;

			case 'hexa77'
				c.microlensType        = 'hexa';
				c.microlensCount       = [471 362];
				c.microlensPitch       = 0.0770;
				c.microlensFocalLength = 0.3080;

			case 'hexa35.5'
				c.microlensType        = 'hexa';
				c.microlensCount       = [1014 780];
				c.microlensPitch       = 0.0355;
				c.microlensFocalLength = 0.1065;

			% Something went wrong
			otherwise
				error('Unrecognized camera component: %s\n',varargin{k});

		end
	end
end
