% Refocus each calibration target to its respective plane. The resulting
% set of images should have no wobble in the dots.
clear variables; close all; clc;

imageDir = 'D:\Project\Data\lfcal\camera0\';
zVals = 0:5:50;

mcalFile  = 'D:\Projects\Data\mcal\camera0.drg-mcal';
lfcalFile = 'D:\Projects\Data\lfcal\camera0.drg-lfcal';
settingsFile = [imageDir 'settings.txt'];

addpath(genpath( 'C:\Code\Dragon\MATLAB' ));  % add Dragon functions to path
DragonFilePath = 'C:\Code\Dragon\Library';    % path to dragon.dll

% Load Dragon Library
isDragonLoaded = DragonLoadLibrary(DragonFilePath);

% Settings Init
settings = DragonGetDefaultSettings('hexa'); % helper function to init all settings values

% Reconstruction Settings
xMin = -20; xMax = 40;
yMin = -20; yMax = 20;

settings.nVoxelsX   = 900;
settings.nVoxelsY   = 600;

settings.algorithm  = 0;  % 1 is filtered refocusing  
settings.f_m        = 60;
settings.M          = -0.76;
settings.fnum_m     = 2.45;

DragonWriteSettingsFile(settingsFile, settings)
DragonInitFromSettingsFile(settingsFile);

%  Read in Volumetric Calbration Coefficients
[xCoeff yCoeff] = DragonLoadDLFC(lfcalFile);

% Construct x,y vectors from settings
xVec = linspace( xMin, xMax, settings.nVoxelsX );
yVec = linspace( yMin, yMax, settings.nVoxelsY );


for ind = 1:numel(zVals)
    zVal = zVals(ind);

    imageFile = sprintf( '%s\\%dmm\\average.tif', imageDir, zVal );
    rawImages = 65535*imadjust(im2double(imread(imageFile)));
    
    fprintf('Calculating Light Field...')
    DragonBuildRadiance(mcalFile,rawImages);
    fprintf('\t\tComplete.\n')
    
    image = DragonReconstructToImage(xVec,yVec,zVal,1,xCoeff,yCoeff);
    imwrite(image'/max(image(:)),[imageDir '\z' num2str(zVal) '.png']);
    pause(1)
end

DragonQuit;