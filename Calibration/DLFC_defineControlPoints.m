% Find calibration points for DLFC
% Tim Fahringer
% 10-26-2016
%#ok<*AGROW>
clear variables; close all; clc;


%-------------------------------------------------------------------
%% Inputs
%-------------------------------------------------------------------

depthLocation = 25;     % mm
centerCoords  = [0 0];  % (x,y) coordinates for the center point, mm
mainDir       = 'D:\Project\Data\lfcal\camera0';

depthDir       = fullfile( mainDir, sprintf('%gmm',depthLocation) );
perspectiveDir = fullfile( depthDir, 'Perspectives' );
resultsDir     = dirchk(fullfile( mainDir, 'Results', filesep ));
outputFile     = fullfile( resultsDir, sprintf('%+g.drg-dots',depthLocation) );

cropX = [1 900];
cropY = [1 600];
thresholdWindow = 60; % px
M = -0.76;
f_m = 60;

uvSamplingFileName = fullfile(mainDir,'uvSamples.txt');
processingSuite = 'Dragon'; % 'Dragon' or 'LFIT'

% Dot card info
dotCardType = 1;   % 0 for single plane, 1 for multi-plane LaVision dot card
dotLocators = 0;   % 0 for no locators, 2 for square and triangle, N for other config
dotPolarity = 1;   % 0 for black dots, white background, 1 for opposite.
dotRadiiEst = 10;  % px
dotSpacing  = 10;  % mm

dotStepSelection = 0;  % 0 for control points on alternate surface
dotStepDepth     = 2;  % mm, use -2 for z-inverted calibration


%-------------------------------------------------------------------
%% Read in STUV data
%-------------------------------------------------------------------
if strcmpi(processingSuite,'lfit')
    stuvDataFileName = 'D:\Project\Data\lfcal\camera0\stuvData.mat';
    
    load(stuvDataFileName);  % sSSRange, tSSRange, sizePixelAperture
    nPixelsX = length(sSSRange);
    nPixelsY = length(tSSRange);
else
    % build s,t sampling
    nPixelsX = 900;
    nPixelsY = 600;
    
    sSSRange = zeros(nPixelsX,1);
    tSSRange = zeros(nPixelsY,1);
    
    xMin = -(6600 * 0.0055) / 2.0;
    xMax =  (6600 * 0.0055) / 2.0;
    dx = (xMax - xMin) / (nPixelsX - 1.0);
    for i = 1:nPixelsX
        sSSRange(i) = xMin + dx * (i-1);
    end
    
    yMin = -(4400 * 0.0055) / 2.0;
    yMax =  (4400 * 0.0055) / 2.0;
    dy = (yMax - yMin) / (nPixelsY - 1.0);
    for i = 1:nPixelsY
        tSSRange(i) = yMin + dy * (i-1);
    end
    
    s_i = (1 -M)*f_m;
    f_l = 0.308;
    p_p = 0.0055;
    sizePixelAperture = s_i * p_p / f_l;
end

fileID = fopen(uvSamplingFileName,'r');
uvSampling = fscanf(fileID,'%d %d',[2 inf]);
fclose(fileID);
nTotalSamplesUV = size(uvSampling,2);

uRange = uvSampling(1,:)' * sizePixelAperture;
vRange = uvSampling(2,:)' * sizePixelAperture;


%-------------------------------------------------------------------
%% Read in Perspective images
%-------------------------------------------------------------------
perspectiveImages = zeros(length(sSSRange),length(tSSRange),nTotalSamplesUV);
for uv = 1:nTotalSamplesUV
    perspectiveImages(:,:,uv) = double(imread( fullfile( perspectiveDir, sprintf('%04d.tif',uv-1) ) ))';
end


%-------------------------------------------------------------------
%% Select control points
%-------------------------------------------------------------------
xControlPoints = zeros(5,1);
yControlPoints = zeros(5,1);
n = 1;

fh = figure;
imagesc((1-2*dotPolarity)*perspectiveImages(:,:,1)'); hold on
colormap jet
axis image

if exist('centerDot.mat','file')
    load('centerDot.mat');
    plot(centerDot(1),centerDot(2),'r+','MarkerSize',15);
end

title(sprintf('Select center (%g,%g) point',centerCoords(1),centerCoords(2)));
[x,y] = dotselect; xControlPoints(n) = x; yControlPoints(n) = y; n = n + 1;
plot(x,y,'wx','MarkerSize',15);
centerDot2 = [xControlPoints(1),yControlPoints(1)];

title('Select TOP LEFT point');
[x,y] = dotselect; xControlPoints(n) = x; yControlPoints(n) = y; n = n + 1;
plot(x,y,'w+','MarkerSize',15);

title('Select TOP RIGHT point');
[x,y] = dotselect; xControlPoints(n) = x; yControlPoints(n) = y; n = n + 1;
plot(x,y,'w+','MarkerSize',15);

title('Select BOTTOM LEFT point');
[x,y] = dotselect; xControlPoints(n) = x; yControlPoints(n) = y; n = n + 1;
plot(x,y,'w+','MarkerSize',15);

title('Select BOTTOM RIGHT point');
[x,y] = dotselect; xControlPoints(n) = x; yControlPoints(n) = y; n = n + 1;
plot(x,y,'w+','MarkerSize',15);

for k=1:dotLocators
    title(sprintf('Select locator %g',k));
    [x,y] = dotselect; xControlPoints(n) = x; yControlPoints(n) = y; n = n + 1;
    plot(x,y,'rx')
end

nDotsX = input('Input number of dots, including the ones selected, spanning the control points in the x direction: ');
nDotsY = input('Input number of dots, including the ones selected, spanning the control points in the y direction: ');
try close(gcf); end
pause(0.01);


%-------------------------------------------------------------------
%% Find dot locations
%-------------------------------------------------------------------
estimatedArea = 0.65*pi*dotRadiiEst^2;

xLocImg = [];
yLocImg = [];
xLocAct = [];
yLocAct = [];
zLocAct = [];
uLoc = [];
vLoc = [];

for uv = 1:nTotalSamplesUV
    % Get individual image and set values between 0 -> 1
    I = perspectiveImages(cropX(1):cropX(2),cropY(1):cropY(2),uv)./max(perspectiveImages(:));
    
    if dotPolarity
        I = imcomplement(I);
    end
    
    % Convert to binary image
    Ibw = adaptivethreshold(I,thresholdWindow,0);
    Ibw = imfill(~Ibw,'holes');

%   if uv==1,  fh = figure;
%   else       clf(fh);
%   end
%   imagesc(Ibw'); pause(0.1);
%   if uv==nTotalSamplesUV,  close(fh);
%   end
    
    % Find centroids
    Ilabel = logical(Ibw);
    stat = regionprops(Ilabel,'Centroid', 'FilledArea');
    
    xDot = []; yDot = [];
    for x = 1:numel(stat)
        if (stat(x).FilledArea > estimatedArea && stat(x).Centroid(1) > 2*dotRadiiEst && stat(x).Centroid(1) < size(I,2) - 2*dotRadiiEst &&...
                stat(x).Centroid(2) > 2*dotRadiiEst && stat(x).Centroid(2) < size(I,1) - 2*dotRadiiEst)
            xDot = [xDot; stat(x).Centroid(2) + cropX(1)-1];
            yDot = [yDot; stat(x).Centroid(1) + cropY(1)-1];
        end
    end
    
    % Determine closest dots to control points
    nControlPoints = 5 + dotLocators;
    controlPointsIdx = zeros(nControlPoints,1);
    for i = 1:nControlPoints
        [~,controlPointsIdx(i)] = min(sqrt( (xControlPoints(i) - xDot).^2 + (yControlPoints(i) - yDot).^2 ));
    end
    
    % Sort points
    originX = xDot(controlPointsIdx(1));
    originY = yDot(controlPointsIdx(1));
    
    topLeftX = xDot(controlPointsIdx(2));
    topLeftY = yDot(controlPointsIdx(2));
    
    topRightX = xDot(controlPointsIdx(3));
    topRightY = yDot(controlPointsIdx(3));
    
    bottomLeftX = xDot(controlPointsIdx(4));
    bottomLeftY = yDot(controlPointsIdx(4));
    
    bottomRightX = xDot(controlPointsIdx(5));
    bottomRightY = yDot(controlPointsIdx(5));
    
    theta_top    = atan2(topRightY - topLeftY,       topRightX - topLeftX);
    theta_bottom = atan2(bottomRightY - bottomLeftY, bottomRightX - bottomLeftX);
    theta_left   = atan2(bottomLeftX - topLeftX,     bottomLeftY - topLeftY);
    theta_right  = atan2(bottomRightX - topRightX,   bottomRightY - topRightY);
    
    dotSpacing_top    = abs(topLeftX - topRightX) / (nDotsX-1);
    dotSpacing_bottom = abs(bottomLeftX - bottomRightX) / (nDotsX-1);
    dotSpacing_left   = abs(topLeftY - bottomLeftY) / (nDotsY-1);
    dotSpacing_right  = abs(topRightY - bottomRightY) / (nDotsY-1);
    
    %     figure;
    %     imagesc(perspectiveImages(:,:,1)')
    %     colormap gray
    %     axis image
    %     hold on
    
    for i = 1:length(xDot)
        %         if ~(i == controlPointsIdx(6))% || i == controlPointsIdx(7))
        
        %         plot(xDot(i),yDot(i),'bo')
        
        % Get angles/spacing at dot location
        y0 = topLeftY    + xDot(i) * tan(theta_top);
        y1 = bottomLeftY + xDot(i) * tan(theta_bottom);
        
        theta_x = theta_top + (theta_bottom - theta_top) * (yDot(i) - y0)/(y1 - y0);
        dotSpacing_x = dotSpacing_top + (dotSpacing_bottom - dotSpacing_top) * (yDot(i)  - y0)/(y1 - y0);
        
        x0 = topLeftX  + yDot(i) * tan(theta_left);
        x1 = topRightX + yDot(i) * tan(theta_right);
        
        theta_y = theta_left + (theta_right - theta_left) * (xDot(i) - x0)/(x1 - x0);
        dotSpacing_y = dotSpacing_left + (dotSpacing_right - dotSpacing_left) * (xDot(i) - x0)/(x1 - x0);
        
        xDist = ((xDot(i) - originX)/cos(theta_y) - ( ((yDot(i)-originY) - (xDot(i) - originX)*tan(theta_x)) * tan(theta_y)));
        yDist = (((yDot(i) - originY) - (xDot(i) - originX)*tan(theta_y))/cos(theta_x));
        
        xIdx = round( xDist/dotSpacing_x );
        yIdx = round( yDist/dotSpacing_y );
        
        if dotCardType % multi-step dot card
            if abs(yDist / dotSpacing_y - round(yDist / dotSpacing_y )) > 0.2
                xIdx = ceil( xDist/dotSpacing_x );
                yIdx = ceil( yDist/dotSpacing_y );
                
                % dot is on other level
                xIdx = xIdx - 0.5;
                yIdx = yIdx - 0.5;
                
                zLocAct = [zLocAct; depthLocation + (1-dotStepSelection)*dotStepDepth];
            else
                zLocAct = [zLocAct; depthLocation + dotStepSelection*dotStepDepth];
            end
        else
            zLocAct = [zLocAct; depthLocation];
        end
        
        xLocImg = [xLocImg; interp1(1:nPixelsX,sSSRange,xDot(i))];
        yLocImg = [yLocImg; interp1(1:nPixelsY,tSSRange,yDot(i))];
        
        xLocAct = [xLocAct; xIdx * dotSpacing - centerCoords(1)];
        yLocAct = [yLocAct; yIdx * dotSpacing - centerCoords(2)];
        
        uLoc = [uLoc; uRange(uv)];
        vLoc = [vLoc; vRange(uv)];
        %         end
    end
    
    xControlPoints(1) = xDot(controlPointsIdx(1));
    xControlPoints(2) = xDot(controlPointsIdx(2));
    xControlPoints(3) = xDot(controlPointsIdx(3));
    xControlPoints(4) = xDot(controlPointsIdx(4));
    xControlPoints(5) = xDot(controlPointsIdx(5));
    
    yControlPoints(1) = yDot(controlPointsIdx(1));
    yControlPoints(2) = yDot(controlPointsIdx(2));
    yControlPoints(3) = yDot(controlPointsIdx(3));
    yControlPoints(4) = yDot(controlPointsIdx(4));
    yControlPoints(5) = yDot(controlPointsIdx(5));
end


%-------------------------------------------------------------------
%% Unify dot clusters
%-------------------------------------------------------------------
% % Find each cluster
% list = 1:length(xLocImg);
% i = 1;
% while ~isempty(list)
% 	dist = sqrt( (xLocImg(list)-xLocImg(list(1))).^2 + (yLocImg(list)-yLocImg(list(1))).^2 );
% 	neighbors = find( dist < 1 );
% 	
% 	cluster{i} = list(neighbors);
% 	list(neighbors) = [];
% 	
% 	i = i+1;
% end
% 
% % Loop through the clusters
% for i=1:length(cluster);
% 	% Determine how many assignments there were
% 	xx = unique( xLocAct(cluster{i}) );
% 	yy = unique( yLocAct(cluster{i}) );
% 	
% 	% Unify the assignments in x
% 	nx = zeros(1,numel(xx));
% 	for j=1:numel(xx)
% 		nx(j) = sum( xLocAct==xx(j) );  % How many of each
% 	end
% 	[~,k] = max(nx);
% 	xLocAct(cluster{i}) = xx(k);
% 	
% 	% Unify the assignments in y
% 	ny = zeros(1,numel(yy));
% 	for j=1:numel(yy)
% 		nx(j) = sum( yLocAct==yy(j) );  % How many of each
% 	end
% 	[~,k] = max(ny);
% 	yLocAct(cluster{i}) = yy(k);
% end


%-------------------------------------------------------------------
%% Display dots for debugging
%-------------------------------------------------------------------
fh = figure;
while true
        
    clf(fh); hold on;
    title({'Each cluster should have uniform color and shape.';'LMB to delete, RMB to inspect, MMB to alter, Enter to stop.'});
    
    xx = unique(xLocAct);
    yy = unique(yLocAct);
    for m = 1:numel(xx)
        for n = 1:numel(yy)
            % By plotting in chunks, each cluster is colored by its
            % physical assignment. Misassignments will appear as
            % multicolored clusters.
            k = ( xLocAct==xx(m) & yLocAct==yy(n) );
            scatter( xLocImg(k), yLocImg(k) );
        end
    end

    [xi,yi,button] = dotselect(1,'square');
    if isempty(xi)
        break;
    elseif button==3  % Right click
        k = ( abs(xLocImg-xi)<1 & abs(yLocImg-yi)<1 );
        fprintf( 'Detected the following x: %s\n', num2str(unique( xLocAct(k) )','%8.2f') );
        fprintf( 'Detected the following y: %s\n', num2str(unique( yLocAct(k) )','%8.2f') );
        fprintf( 'Detected the following z: %s\n', num2str(unique( zLocAct(k) )','%8.2f') );
        fprintf( '\n' );
    elseif button==2  % Middle Click
        k = ( abs(xLocImg-xi)<1 & abs(yLocImg-yi)<1 );
        xLocAct(k) = input('Resssign x: ');
        yLocAct(k) = input('Reassign y: ');
        zLocAct(k) = input('Reassign z: ');
        fprintf( '\n' );
    else
        k = ( abs(xLocImg-xi)<0.5 & abs(yLocImg-yi)<0.5 );
        xLocImg(k) = [];
        yLocImg(k) = [];
        xLocAct(k) = [];
        yLocAct(k) = [];
        zLocAct(k) = [];
        uLoc(k) = [];
        vLoc(k) = [];
    end

end%while
close(fh);


%-------------------------------------------------------------------
%% Output dot locations to file
%-------------------------------------------------------------------
fileID = fopen(outputFile,'wt');
for i = 1:length(xLocImg)
    fprintf( fileID, '%f,%f,%f,%f,%f,%f,%f\n', xLocImg(i),yLocImg(i), xLocAct(i),yLocAct(i),zLocAct(i), uLoc(i),vLoc(i) );
end
fclose(fileID);

centerDot = centerDot2;
save('centerDot.mat','centerDot')
