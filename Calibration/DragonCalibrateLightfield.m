function DragonCalibrateLightfield
%DRAGONCALIBRATELIGHTFIELD is not a function.
%   To perform light-field calibration on a series of calibration images, edit and run the following scripts in order:
%      1. Use `DLFC_generatePerspectives` to create a batch of perspective views for each calibration depth.
%      2. Use `DLFC_defineControlPoints` to extract the dot locations from each calibration.
%      3. Use `DLFC_calculateCoeffs` to find the calibration coefficients.
%      4. Use `DLFC_verifyCalibration` to visually inspect the calibration accuracy.

help DragonCalibrateLightfield