% Generate Perspectives for Light Field Calibration
clear variables; clc;


%% SETUP

addpath(genpath( 'C:\Code\Dragon\MATLAB' ));  % add Dragon functions to path
DragonFilePath = 'C:\Code\Dragon\Library';    % path to dragon.dll

% Inputs
mainDir  = 'D:\Project\Data\lfcal\camera0';
mcalFile = 'D:\Project\Data\mcal\camera0.drg-mcal';

% Experimental details
f_m    = 60;      % Main lens focal length [mm]
M      = -0.76;   % Magnification
fnum_m = 2.45;    % Main lens f-number
zVals  = 0:5:50;  % Calibration plate z-depths [mm]


%% INITIALIZE DRAGON

% Initialize settings file
settings     = DragonGetDefaultSettings('hexa');  % helper function to init all settings values
settingsFile = fullfile(mainDir,'settings.txt');
DragonWriteSettingsFile(settingsFile,settings);

% Load Dragon and initialize global settings
if DragonLoadLibrary(DragonFilePath)
    DragonInitFromSettingsFile(settingsFile);
else
    error('Dragon Library not loaded. Ensure that the filepath provided to DragonLoadLibrary was correct.');
end

% Update settings (from ExampleDataInfo.txt)
settings.f_m    = f_m;
settings.M      = M;
settings.fnum_m = fnum_m;

DragonWriteSettingsFile(settingsFile,settings)
DragonInitFromSettingsFile(settingsFile);


%% GENERATE PERSPECTIVES AT EACH Z-DEPTH

for ind = 1:numel(zVals)
    zVal = zVals(ind);
    
    zDir      = fullfile(mainDir,sprintf('%02dmm',zVal));
    imageFile = fullfile(zDir,'average.tif');
    saveDir   = dirchk(fullfile(zDir,'Perspectives',filesep));

    % Load raw image
    rawImage = double(imread(imageFile));
    
    % Build radiance arrays
    DragonBuildRadiance(mcalFile,rawImage);
    
    % Generate x,y data based on CCD
    xVec = linspace( -settings.nPixelsX*settings.p_p/2, settings.nPixelsX*settings.p_p/2, 900 );
    yVec = linspace( -settings.nPixelsY*settings.p_p/2, settings.nPixelsY*settings.p_p/2, 600 );
    
    % Get perspective sweep
    si = (1 - settings.M)*settings.f_m;
    sizeOfPixelOnAperture = si * settings.p_p / settings.f_l;
    [uVec,vVec] = turtle(7^2,mainDir);
    uVec = uVec*sizeOfPixelOnAperture;
    vVec = vVec*sizeOfPixelOnAperture;
    
    perspectiveSweep = DragonGeneratePerspectiveViews( xVec,yVec, uVec,vVec );
    
    % Output perspective sweep
    for i = 1:length(uVec)
        imwrite( uint16(perspectiveSweep(:,:,i)'), fullfile(saveDir,sprintf('%04d.tif',i-1)) );
    end
end

