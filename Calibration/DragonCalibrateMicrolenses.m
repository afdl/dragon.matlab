function DragonCalibrateMicrolenses(image,filename)
%DRAGONCALIBRATEMICROLENSES


%% FIND MICROLENSES
if ischar(image), image = imread(image); end


%% SMOOTH VIA REGRESSION
% Generate reference grid
i = 1:numS;
j = 1:numT;
[i,j] = ndgrid(i,j);

% 
if hexa && exactS(1,2)>exactS(2,2)
	i(:,2:2:end) = i(:,2:2:end) + 0.5;
elseif hexa
	i(:,1:2:end) = i(:,1:2:end) + 0.5;
end

% Fit surfaces to (s,t)
fs = fit( [i(:),j(:)], exactS(:), 'poly22' );
ft = fit( [i(:),j(:)], exactT(:), 'poly22' );

% 
smoothS = fs(i,j);
smoothT = ft(i,j);

%% SAVE
[~,name,ext] = fileparts(filename);
switch lower(ext)
	case '.drg-mcal' % Dragon ASCII
		fid = fopen( fileOut, 'w' );
		fprintf( fid, '%d\n', numS );
		fprintf( fid, '%d\n', numT );
		for k=1:numel(exactS)
			fprintf( fid, '%f,%f\n', smoothS(k),smoothT(k) );
		end
		fclose(fid);
		
	case '.mat' % LFIT-compatible MATLAB binary
		mcal.numS   = numS;
		mcal.numT   = numT;
		mcal.exactX = smoothS;
		mcal.exactY = smoothT;
		mcal.roundX = round(smoothS);
		mcal.roundY = round(smoothT);
		save(filename,'-struct','mcal');
		
end