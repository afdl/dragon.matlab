function DragonCreateWaveletBG( lengthScales, paperSize, paperMargin )
%DRAGONCREATEWAVELETBG Creates a full page wavelet-noise background.
%	Create a PDF containing a wavelet-noise background containing the
%	specified length scales. The input must contain  two or more length
%	scales, where the smallest scale is one pixel in resolution and the
%	largest scale is the size of the largest wavelet. Using more than two
%	length scales results in more "texture" for the final background.
%
%	Examples:
%		DragonCreateWaveletBG( [0.2 30] ) creates a background with 30 mm 
%			wavelets on a 0.2 mm grid.
%		DragonCreateWaveletBG( [0.2 30 40] ) create a background with 30
%			and 40 mm wavelets on a 0.2 mm grid.

if nargin<1, lengthScales = [0.2 30];      end  % Default length scales
if nargin<2, paperSize    = [215.9 279.4]; end  % Default paper size
if nargin<3, paperMargin  = 10;            end  % Default paper margins


canvasSize.mm = paperSize - 2*paperMargin;
canvasSize.px = round( canvasSize.mm / min(lengthScales) );

tileSize  = max(canvasSize.mm)./lengthScales(lengthScales~=min(lengthScales));
tileSize1 = ceil( tileSize );
M = length(tileSize1);  % number of wavelet sizes

cropX = ( 1 - canvasSize.mm(1)/max(canvasSize.mm)*tileSize./tileSize1 )/2;
cropY = ( 1 - canvasSize.mm(2)/max(canvasSize.mm)*tileSize./tileSize1 )/2;

bg = zeros(canvasSize.px)';

% Generation of random noise for the declared tile sizes. This for loop
% also downsamples and upsamples these random noise tiles. The modified
% tiles are then subtracted from the original noise tiles to effectively
% generating high pass filtered images. These resulting noise tiles are
% then upsampled again to the final tile background size and added
% together.
for m = 1:M
	N = 2^tileSize1(m);                   % new tile 2^j x 2^j
	r = rand(N,N);

	for n = 1:N
		r_d(n,:) = downsample(r(n,:));    % downsampling rows
		r_u(n,:) = upsample(r_d(n,:));    % upsampling rows
	end

	for n = 1:N
		c_d(:,n) = downsample(r_u(:,n));  % downsampling columns
		c_u(:,n) = upsample(c_d(:,n));    % upsampling columns
	end

	r1 = r - c_u;                         % subtract modified tile from original

	[x,y] = meshgrid( linspace( 1+cropX(m)*N, N-cropX(m)*N, canvasSize.px(1) ), ...
	                  linspace( 1+cropY(m)*N, N-cropY(m)*N, canvasSize.px(2) ) );
	bg = bg + interp2(r1,x,y,'bicubic');  % add current tile to total

	clear r r1 r_d r_u c_d c_u
end

fh = figure( ...
	'Visible', 'off', ...
	'PaperUnits', 'centimeters', ...
	'PaperSize', paperSize/10, ...
	'PaperPosition', [paperMargin paperMargin canvasSize.mm]/10 );
ah = axes( fh, ...
	'Position', [0 0 1 1], ...
	'Units', 'normalized', ...
	'Visible', 'off' ); 
imagesc(ah,bg); colormap(gray); axis off;
print(fh,'waveletBG.pdf','-dpdf','-r0');
close(fh);

end%function


function B = upsample(A)
% This function upsamples the each given row and column individually with
% the coefficients defined by Cook et al. in their paper "Wavelet Noise"

p_coeffs = [0.25, 0.75, 0.75, 0.25];

n = length(A)*2;
B = zeros(1,n);
for i = 0:n-1
	for k = fix(i/2):fix(i/2)+1
		B(i+1) = B(i+1) + p_coeffs(i-2*k+3) * A(mod(k,n/2)+1);
	end
end

end%function


function B = downsample(A)
% This function downsamples the each given row and column individually with
% the coefficients defined by Cook et al. in their paper "Wavelet Noise"

a_coeffs = [ 0.000334,-0.001528, 0.000410, 0.003545,-0.000938,-0.008233, 0.002172, 0.019120,...
            -0.005040,-0.044412, 0.011655, 0.103311,-0.025936,-0.243780, 0.033979, 0.655340,...
             0.655340, 0.033979,-0.243780,-0.025936, 0.103311, 0.011655,-0.044412,-0.005040,...
             0.019120, 0.002172,-0.008233,-0.000938, 0.003546, 0.000410,-0.001528, 0.000334 ];

n    = length(A);
arad = length(a_coeffs)/2;
B    = zeros(1,n/2);
for i = 0:(n/2-1)
	for k = 2*i-arad:2*i+arad-1
		B(i+1) = B(i+1) + a_coeffs((k-2*i)+17) * A(mod(k,n)+1);
	end
end

end%function