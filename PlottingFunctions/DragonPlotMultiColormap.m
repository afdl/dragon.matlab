function h = DragonPlotMultiColormap(x1,y1,backgroundImage,x2,y2,forgroundImage,cmin_2,cmax_2)

% Define colormap
nElements = 256;  % 64-elements is each colormap
colormap([gray(nElements);jet(nElements)])

mask = forgroundImage;
mask(mask ~= 0) = 1;

forgroundImage = forgroundImage - min(forgroundImage(:));

% Create figure
h(1) = pcolor(x1,y1,backgroundImage); hold on
h(2) = pcolor(x2,y2,forgroundImage); hold off
alpha(h(2),mask)
shading flat

% move the refocused image to Z = 10; (in front of perspective image)
set(h(2),'ZData',forgroundImage + 10)

% Create coloraxis data 1->nElements (perspective view), nElements + 1 -> 2*nElements (refocus)
cmin_1 = min(backgroundImage(:));
cmax_1 = max(backgroundImage(:));
if nargin == 6
    cmin_2 = min(forgroundImage(:));
    cmax_2 = max(forgroundImage(:));
end

C1 = min(nElements,round((nElements-1)*(backgroundImage-cmin_1)/(cmax_1-cmin_1))+1); 
C2 = min(nElements,round((nElements-1)*(forgroundImage-cmin_2)/(cmax_2-cmin_2))+1) + nElements; 

% set colormap data for each image
set(h(1),'CData',C1);
set(h(2),'CData',C2);

% set overall colormap range
caxis([1 2*nElements])

% make it look like an image
set(gca,'YDir','reverse')
axis image