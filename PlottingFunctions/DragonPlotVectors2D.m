function [h] = DragonPlotVectors2D(x, y, u, v, isMasked, var, xLabel, yLabel, cLabel)

% Create figure
h = figure('color','w');
 
% Get axes of figure
ax = gca;

% smooth var
fil = 1/9*ones(3);
var = filter2(fil,var);

hold on
% Plot contour of var, quiver of u,v and image mask 
contourf(ax,x,y,var)
quiver(x,y,u,v,'color',[0 0 0])
imagesc(x(:,1),y(1,:),isMasked','CData',zeros(size(isMasked',1),size(isMasked',2),3),'AlphaData',isMasked');
hold off

% Reverse y axis (look like an image) and set plot parameters
set(ax,'YDir','reverse')
c = colorbar(ax);
axis(ax,'image')
colormap(ax,'jet')
%caxis(ax,[-0.5,1.5])
xlabel(xLabel,'FontSize',12,'Interpreter','LaTeX')
ylabel(yLabel,'FontSize',12,'Interpreter','LaTeX')
ylabel(c,cLabel,'FontSize',12,'Interpreter','LaTeX')

