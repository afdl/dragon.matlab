function [hxy, hyz] = DragonPlotVolumeProjections(x, y, z, intensity, xPart, yPart, zPart, xLabel, yLabel, zLabel)

intensity = smooth3(intensity);

% Create XY projection
hxy = figure('color','w');

hold on
projXY(:,:) = sum(intensity,3);
imagesc(x,y,projXY');
colormap gray
colormap(flipud(colormap))
plot(xPart,yPart,'ro')
hold off
xlim([min(x),max(x)])
ylim([min(y),max(y)])
xlabel(xLabel)
ylabel(yLabel)
axis image

% Create YZ projection
hyz = figure('color','w');

hold on
projYZ(:,:) = sum(intensity,1);
imagesc(y,z,projYZ');
colormap gray
colormap(flipud(colormap))
plot(yPart,zPart,'ro')
hold off
xlim([min(y),max(y)])
ylim([min(z),max(z)])
xlabel(yLabel)
ylabel(zLabel)
axis image
