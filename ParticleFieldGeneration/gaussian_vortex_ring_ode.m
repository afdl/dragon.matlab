function [out] = gaussian_vortex_ring_ode(t, in, cth, gam, corerad, xc, yc, zc, vx, vy, vz)
%
% Function for a 3-D axisymmetric Gaussian vortex right translating along the y-axis.
%
% Date: 19-6-2012
% Kyle Lynch (k.p.lynch@tudelft.nl)

X=in(1);
Y=in(2);
Z=in(3);

xc = xc + t*vx;
yc = yc + t*vy;
zc = zc + t*vz;

rpoint = sqrt((X-xc).^2 + (Z-zc).^2);
hpoint = Y-yc;
r1 = rpoint-corerad;
r2 = rpoint+corerad;
d1 = sqrt( r1.^2 + hpoint.^2 );
d2 = sqrt( r2.^2 + hpoint.^2 );

omega1 = gam / (2*pi*d1+1E-15) * (1-exp(-d1^2 / cth));
theta1 = atan2(r1, hpoint);

omega2 = -gam / (2*pi*d2+1E-15) * (1-exp(-d2^2 / cth));
theta2 = atan2(r2, hpoint);

Vy = omega1*sin(theta1) + omega2*sin(theta2);
Vrho = omega1*cos(theta1) + omega2*cos(theta2);

alpha = atan2(Z-zc,X-xc);

u = -Vrho*cos(alpha)+vx; 
v = Vy+vy;
w = -Vrho*sin(alpha)+vz;

out=[u; v; w];

end