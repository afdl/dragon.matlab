function vol = DragonCreateVolumeFromParticles(part,xVec,yVec,zVec)

nVoxelsX = length(xVec);
nVoxelsY = length(yVec);
nVoxelsZ = length(zVec);

vol = zeros(nVoxelsX,nVoxelsY,nVoxelsZ);

xval = part.x;
yval = part.y;
zval = part.z;
ival = part.i;

x_num = 1:nVoxelsX;
y_num = 1:nVoxelsY;
z_num = 1:nVoxelsZ;

sigma_x = 0.85;
sigma_y = 0.85;
sigma_z = 0.85;

for p = 1:length(xval)
    
    x0 = xval(p) + 0.077;
    y0 = yval(p) + 0.077;
    z0 = zval(p);
    
    xv = interp1(xVec,x_num,x0);
    yv = interp1(yVec,y_num,y0);
    zv = interp1(zVec,z_num,z0);
    
    for i = -3:3
        for j = -3:3
            for k = -3:3
                
                x = floor(xv) + i;
                y = floor(yv) + j;
                z = floor(zv) + k;
                
                if (x > 1 && x < nVoxelsX+1 && y > 1 && y < nVoxelsY+1 && z > 1 && z < nVoxelsZ+1)
                    vol(x,y,z) = vol(x,y,z) + ival(p)*exp(-1*( (x - xv)^2/(2*sigma_x^2) + ...
                        (y - yv)^2/(2*sigma_y^2) + ...
                        (z - zv)^2/(2*sigma_z^2) ));
                end
                
            end
        end
    end
end