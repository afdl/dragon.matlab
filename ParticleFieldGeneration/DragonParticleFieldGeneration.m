function [partA, partB] = DragonParticleFieldGeneration(nParticles,x,y,z,displacementType,varargin)

    % Generate initial particle field
    partA = generateRandomParticleField(nParticles,x,y,z);

    % Generate displaced particle field
    switch (displacementType)
        case 'uniform'
            [u,v,w] = deal(varargin{:});
            partB = uniformDisplacement(partA,u,v,w);
        case 'ring_vortex'
            [vrad,vx,vy,vz,rc,xc,yc,zc] = deal(varargin{:});
            partB = ringVortexDisplacement(partA,vrad,vx,vy,vz,rc,xc,yc,zc);
        case 'oseen_vortex'    
            [vThetaMax, coreRadius, dt, axis] = deal(varargin{:});
            partB = oseenVortexDisplacement(partA,vThetaMax,coreRadius,dt, axis);
    end
end

function [part] = generateRandomParticleField(nParticles,x,y,z)
    part.x = rand(nParticles,1)*(max(x)-min(x)) + min(x);
    part.y = rand(nParticles,1)*(max(y)-min(y)) + min(y);
    part.z = rand(nParticles,1)*(max(z)-min(z)) + min(z);
    part.d = ones(nParticles,1)*0.010;
    part.i = ones(nParticles,1);
end

function [partB] = uniformDisplacement(partA,u,v,w)
   
    partB.x = partA.x + u;
    partB.y = partA.y + v;
    partB.z = partA.z + w;
    partB.d = partA.d;
    partB.i = partA.i;

end

function [partB] = ringVortexDisplacement(partA,vrad,vx,vy,vz,rc,xc,yc,zc)
   
    dt = 1;
    gam2 = 1.256431;
    gam = 2*pi*rc*vrad*(1-exp(-(rc^2)*gam2));
    cth = rc^2 / gam2;
    corerad = 2*rc;
    
    func = @(tin,xin) gaussian_vortex_ring_ode(tin, xin, cth, gam, corerad, xc, yc, zc, vx, vy, vz);

    for i = 1:length(partA.x)
        [~,xout] = ode45(func,[0 dt],[partA.x(i), partA.y(i), partA.z(i)]);
        partB.x(i) = xout(end,1);
        partB.y(i) = xout(end,2);
        partB.z(i) = xout(end,3);
    end
    
    partB.d = partA.d;
    partB.i = partA.i;

end

function [partB] = oseenVortexDisplacement(partA,vThetaMax,coreRadius,dt)

    

end
