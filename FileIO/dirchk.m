function [ fullpath ] = dirchk( fullpath )
%DIRCHK Check that path exists and create if necessary.

[path file ext] = fileparts( fullpath );
if isempty(ext), path = fullfile(path,file); end
if ~exist( path, 'dir' )
    mkdir( path );
end

end
