function [] = DragonSaveVolume(vol,filename)
%-------------------------------------------------------------------------------------
% function [] = DragonSaveVolume(vol,filename)
%
% Purpose: Outputs volume to file
% 
% INPUTS:       vol:      3D matrix containing volume (x,y,z)
%               filename: name of file to export to (*.bin,*.dat or *.plt)
%                
% OUTPUTS:      none
%-------------------------------------------------------------------------------------
[~,name,ext] = fileparts(filename);

switch lower(ext)	
	case '.bin' % Dragon binary blob

		fileID = fopen(filename,'w');
		fwrite(fileID,size(vol,1),'int32');
		fwrite(fileID,size(vol,2),'int32');
		fwrite(fileID,size(vol,3),'int32');
		fwrite(fileID,vol,'single');
		fclose(fileID);
		
	case '.mask' % Dragon binary blob

		fileID = fopen(filename,'w');
		fwrite(fileID,size(vol,1),'int32');
		fwrite(fileID,size(vol,2),'int32');
		fwrite(fileID,size(vol,3),'int32');
		fwrite(fileID,vol,'uint8');
		fclose(fileID);
		
	case '.dat' % TecPlot ASCII

		[nVoxelsX,nVoxelsY,nVoxelsZ] = size(vol);
		
		fileID = fopen(filename,'wt');
		fprintf(fileID,'TITLE="%s"\n',name);
		fprintf(fileID,'VARIABLES="x""y""z""intensity"\n');
		fprintf(fileID,'ZONE T="3-D"\n');
		fprintf(fileID,'i=%d j=%d k=%d\n',nVoxelsX,nVoxelsY,nVoxelsZ);
		
		for k = 1:nVoxelsZ
			for j = 1:nVoxelsY
				for i = 1:nVoxelsX
					fprintf(fileID,'%d %d %d %f\n',i,j,k,vol(i,j,k));
				end
			end
		end
		fclose(fileID);
		
	case '.plt' % TecPlot binary

		[x,y,z] = ndgrid(1:size(vol,1),1:size(vol,2),1:size(vol,3));
		tdata.title = name;
		tdata.Nvar  = 4;
		tdata.varnames = {'X','Y','Z','intensity'};
		tdata.vformat  = ones(4,1);
		tdata.cubes.x = x;
		tdata.cubes.y = y;
		tdata.cubes.z = z;
		tdata.cubes.v(1,:,:,:) = vol;
		mat2tecplot(tdata,filename);
		
	otherwise
		error('Unrecognized file type.');
		
end
