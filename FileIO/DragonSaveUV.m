function DragonSaveUV( u, v, filename )
%DRAGONSAVEUV Save a list of u,v aperture samples.


[~,~,ext] = fileparts(filename);

switch lower(ext)
	case '.drg-uv'

		fid = fopen(filename,'wt');
		fprintf(fid,'%d\n',numel(u));
		for k = 1:numel(u)
			fprintf(fid,'%f,%f\n',u(k),v(k));
		end
		fclose(fid);
		
	otherwise
		error('Unrecognized file type.');
		
end
