function [xCoeff yCoeff] = DragonLoadDLFC(lfcalFile)
%DRAGONLOADDLFC Load light-field calibration coefficients.

fid = fopen(lfcalFile);
coeffs = fscanf(fid,'%f,%f',[2,56])';
fclose(fid);

if nargout<2
    xCoeff = coeffs;
else
    xCoeff = coeffs(:,1);
    yCoeff = coeffs(:,2);
end
