function [x, y, u, v, isValid, isMasked, snr] = DragonReadVectors2D_PLT(fileName)

fileID = fopen(fileName,'r');
if (fileID)
    header1 = fgetl(fileID);
    header2 = fgetl(fileID);
    header = fgetl(fileID);
    data = textscan(fileID,'%f %f %f %f %f %f %f','delimiter',',');
    fclose(fileID);
    
    header(strfind(header,'=')) = [];
    index1 = strfind(header,'I');
    index2 = strfind(header,'J');
    nVectorsX = sscanf(header(index1(1) + length('I'):end), '%g', 1);
    nVectorsY = sscanf(header(index2(1) + length('J'):end), '%g', 1);
    
    x = zeros(nVectorsX,nVectorsY);
    y = zeros(nVectorsX,nVectorsY);
    u = zeros(nVectorsX,nVectorsY);
    v = zeros(nVectorsX,nVectorsY);
    isValid = zeros(nVectorsX,nVectorsY);
    isMasked = zeros(nVectorsX,nVectorsY);
    snr = zeros(nVectorsX,nVectorsY);
    
    m = 1;
    for j = 1:nVectorsY
        for i = 1:nVectorsX
            x(i,j) = data{1}(m);
            y(i,j) = data{2}(m);
            u(i,j) = data{3}(m);
            v(i,j) = data{4}(m);
            isValid(i,j) = data{5}(m);
            isMasked(i,j) = data{6}(m);
            snr(i,j) = data{7}(m);
            m = m + 1;
        end
    end
    
else 
   fprintf('File: (%s) could not be read\n',fileName); 
end