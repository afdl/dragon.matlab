function json = struct2json(s, fileout, level)
if nargin<3, level = 0; end

% Get list of non-empty fields
fields = fieldnames(s);
fields(cellfun(@(n)isempty(s.(n)),fields)) = [];

% Function for easy tab-indent
indent = @(level)  repmat('\t',1,level);

% Construct JSON
json = '';
json = [json indent(level) '{\n'];
for i = 1:length(fields)
	key = fields{i};
	val = s.(key);
	if isstruct(val) % structures
		if numel(val) > 1 % array of structs
			json = [json indent(level+1) sprintf('"%s": [\n',key)];
			for j = 1:length(val)-1
				json = [json struct2json(val(j),[],level+2) ',\n'];
			end
			json = [json struct2json(val(end),[],level+2)];
			json = [json '\n' indent(level+1) ']'];
		else % single struct
			json = [json indent(level+1) sprintf('"%s": ',key)];
			json = [json struct2json(val,[],level+1)];
		end

	elseif isnumeric(val) % numbers
		if numel(val) > 1 % array of numbers
			json = [json indent(level+1) sprintf('"%s": [',key)];
			for j = 1:length(val)-1
				json = [json sprintf('%g,',val(j))];
			end
			json = [json sprintf('%g]',val(end))];
		else % single number
			json = [json indent(level+1) sprintf('"%s": %g',key,val)];
		end

	elseif islogical(val) % booleans (0 and 1 work, but this is more semantic)
		if (val),  json = [json indent(level+1) sprintf('"%s": true',key)];
		else,      json = [json indent(level+1) sprintf('"%s": false',key)];
		end
		
	else % strings
		json = [json indent(level+1) sprintf('"%s": "%s"',key,strrep(val,'\','/'))];

	end
	if i ~= length(fields)
		json = [json ','];
	end
	json = [json '\n'];
end
json = [json indent(level) '}'];

% Cleanup formatting
json = regexprep(json,' (\\t)+',' ');

% Save JSON to file
if nargin>1 && level==0
	fileID = fopen(fileout,'wt');
	fprintf(fileID,json);
	fclose(fileID);
end
