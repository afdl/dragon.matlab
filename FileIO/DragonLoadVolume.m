function [vol] = DragonLoadVolume( file )
%DRAGONLOADVOLUME Load a vector or scalar volume field from specified file.


[~,~,fileType] = fileparts(file);

switch lower(fileType)
    case '.bin' % DragonPIV

        fid = fopen(file,'rb');
        nI  = fread(fid,1,'int32');
        nJ  = fread(fid,1,'int32');
        nK  = fread(fid,1,'int32');
        raw = fread(fid,'single');
        fclose(fid);

        vol.E = reshape( raw, nI,nJ,nK );

    case '.mask' % DragonPIV

        fid = fopen(file,'rb');
        nI  = fread(fid,1,'int32');
        nJ  = fread(fid,1,'int32');
        nK  = fread(fid,1,'int32');
        raw = fread(fid,'uint8');
        fclose(fid);
        
        vol.E = reshape( raw, nI,nJ,nK );

    case {'.dat','.plt'} % Tecplot

        fid = fopen(file,'r');
        for l=0:4
            line = fgets(fid);
            
            if regexpi( line, '^[\d\-\.]' )       % This row contains data
                break;
            elseif regexpi( line, '^variables' )  % This row contains variable names
                varName = regexpi( line, '"([\w\s]+)"', 'tokens' );
                nVars = length(varName);
            elseif regexpi( line, '^zone' )       % This row contains zone information
                s  = regexpi( line, 'i=(\d+),?\s*j=(\d+),?\s*k=(\d+)', 'tokens' );
                nI = str2double( s{1}{1} );
                nJ = str2double( s{1}{2} );
                nK = str2double( s{1}{3} );
            end
        end
        fclose(fid);

        raw = dlmread( file, ',', l,0 );
        raw = reshape( raw, nI,nJ,nK, nVars );
        
        for n=1:nVars
            name = strrep( varName{n}{1}, ' ', '_' );  % Replace spaces with underscores
            vol.(name) = raw(:,:,:,n);
        end

    otherwise
        error('Unrecognized file type.');
        
end
