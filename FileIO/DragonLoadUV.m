function [u,v] = DragonLoadUV( filename )
%DRAGONLOADUV Load a list of u,v aperture samples.


[~,~,ext] = fileparts(filename);

switch lower(ext)
	case '.drg-uv'

		fid = fopen(filename,'rt');
		nUV = fscanf(fid,'%d',1);
		uv = fscanf(fid,'%f,%f',[2,nUV])';
		fclose(fid);
		
	otherwise
		error('Unrecognized file type.');
		
end

if nargout<2
	u = uv;
else
	u = uv(:,1);
	v = uv(:,1);
end
